<?php

require __DIR__ . '/../vendor/autoload.php';

class TestPresenter extends Nette\Application\UI\Presenter
{
	public function test()
	{
		$data = [
			(object) ["id" => 1],
			(object) ["id" => 2],
			(object) ["id" => 3],
		];

		$grid = new \EaredSeal\DataGrid\Grid();
		$grid->setData($data);
		// definice sloupcu
		$container = $grid->getContainer($this);
		// simple
		$container->addText("easy", "id")->setDesign("badge bg-green");

		// use Closure
		$container->addText("hard", function ($row, \EaredSeal\DataGrid\Controls\Text $text) {
			if($row->id == 1)
			{
				$text->setDesign("badge bg-green");
			}
			else
			{
				$text->setDesign("badge bg-red");
			}
			return $row->id . "%";
		});

		return $grid;
	}
}
$test = new TestPresenter();
echo $test->test()->render();