<?php

/*
require __DIR__ . '/../vendor/autoload.php';


$configurator = new Nette\Configurator;
$configurator->setDebugMode(TRUE);

$configurator->setTempDirectory('/tmp');

$configurator->createRobotLoader()
	->addDirectory(__DIR__)
	->register();

$container = $configurator->createContainer();
*/

class TestPresenter extends Nette\Application\UI\Presenter
{
	public function render()
	{
		$items = array(
			(object) array("id" => 1, "title" => "prvni"),
			(object) array("id" => 1, "title" => "druhy"),
			(object) array("id" => 1, "title" => "treti"),
		);

		$grid = new \EaredSeal\DataGrid\Grid($items, $this);
		$grid->setTemplate(__DIR__ . "/../src/Type/Simple.latte");
		// definice sloupcu
		$container = $grid->getContainer();
		$container->addText("#", "id");
		$container->addText("Title", "title");
		$container->addState("Detail Event", "detail");
		$container->addText("Show from - Start event", function (App\Event $row)
		{
			return $row->date_from->format("d.m.Y") . " - " . $row->date_to->format("d.m.Y");
		});
		$container->addState("Status", "isActive()");
		// akce na radek

		$action = $container->addAction("Action");
		$action->addLink("Edit", "edit", "id")->setIco("fa fa-edit");
		$action->addLink("Delete", "delete", "id")->setIco("glyphicon glyphicon-remove")->setConfirm("opravdu chces smazat tento zaznam?");
		$action->addLink("Active", "active!", "id")->setIco("glyphicon glyphicon-play");
		$action->addLink("Deactive", "deactive", "id")->setIco("glyphicon glyphicon-pause");

		$grid->render();
	}
}


$testGrid = new TestPresenter();
$testGrid->render();