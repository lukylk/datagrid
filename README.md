EaredSeal/DataGrid
======


Requirements
------------

EaredSeal/DataGrid requires PHP 5.5.x or higher

- [Nette Framework](https://github.com/nette/nette)
- [Kdyby/Translation](https://github.com/Kdyby/Translation)
- [gruntjs/Grunt](https://github.com/gruntjs/grunt)


Example
------------

```php
final class HomepagePresenter extends Nette\Application\UI\Presenter
{

	/** @var EaredSeal\DataGrid\IGrid @inject */
	public $gridControl;

	public function renderDefault()
	{
		$rules = $this['list']->getSortingRules();
		$searchQuery = $this['list']->getSearchQuery();
		$this['list']->setData($this->userManager->findBy(["query" => $searchQuery], $rules->getRule()));
	}

	public function createComponentList()
	{
		$grid = $this->gridControl->create();
		$grid->setTranslator($this->translator);
		$grid->setLinkSearch("this");

		$container = $grid->getContainer($this);
		$container->addText("#", "id");
		$container->addText("user.form.name", 'name');
		$container->addText("user.form.login", 'login');
		$container->addText("user.form.email", 'email');
		$container->addText("user.form.country", "getCountry()->getName()");
		$container->addText("user.form.role", "role");
		$container->addState("user.form.active", "isActive()");

		$action = $container->addAction('Action'); //->setWidth("28%");
		$action->setTypeMenu($action::TypeMenu_ButtonGroup);
		$action->addLink("Edit", "edit")->setIco("fa fa-edit")->addModal();
		$action->addLink("Active", "setActive!");
		$action->addLink("Deactive", "setDeactive!");
		return $grid;
	}
}
```