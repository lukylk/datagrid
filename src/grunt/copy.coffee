#npm update caniuse-db for update DB!
'use strict';
module.exports = 

	## create backend	
	datagridCssLibs:
		files: [
			# css + imgages
			{expand: true, cwd: '<%= paths.vendor %>/earedseal/datagrid/src/assets/datatables/css/', src: ['**'], dest: '<%= paths.backend %>/css/datatables/'}			
		]
	
	datagridJsLibs:
		files: [
			# jquery dataTables
			{expand: true, cwd: '<%= paths.vendor %>/earedseal/datagrid/src/assets/datatables/js/', src: ['**'], dest: '<%= paths.backend %>/js/datatables/'}
		]		