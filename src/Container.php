<?php

namespace EaredSeal\DataGrid;

use Nette,
	EaredSeal\DataGrid\Controls,
	Kdyby\Translation\ITranslator;

/**
 * Definice sloupcu v tabulce
 */
class Container extends Nette\Application\UI\Control
{

	/** @var \Kdyby\Translation\ITranslator */
    private $translator;

	/** @var array */
	private $controls = array();

	public function createComponentAction()
	{
		return new Controls\Action;
	}

	public function setTranslator(ITranslator $translator = NULL)
	{
		$this->translator = $translator;
		$this['action']->setTranslator($translator);
	}

	/**
	 * @param string $name
	 * @param mix|\Closure $columnCallback
	 * @return \EaredSeal\DataGrid\Controls\Text
	 */
	public function addText($name, $columnCallback)
	{
		$name = $this->translateByName($name);
		$control = new Controls\Text($name, $columnCallback);
		return $this->controls[$name] = $control;
	}

	/**
	 *
	 * @param string $name
	 * @param mix|\Closure $column
	 * @param string $format d.m.Y H.i.s
	 * @return \EaredSeal\DataGrid\Controls\Text
	 */
	public function addDateTime($name, $column, $format = "d.m.Y")
	{
		$name = $this->translateByName($name);
		$control = new Controls\DateTime($name, $column);
		$control->setFormat($format);
		return $this->controls[$name] = $control;
	}

	/**
	 * vyhodnoceni stavu zaznamu, napr. active/deactive
	 * @param type $name
	 * @param type $column
	 * @return \EaredSeal\DataGrid\Controls\State
	 */
	public function addState($name, $column)
	{
		$name = $this->translateByName($name);
		$control = new Controls\State($name, $column);
		return $this->controls[$name] = $control;
	}

	/**
	 * @param string $name
	 * @param mix|\Closure $columnCallback
	 * @return \EaredSeal\DataGrid\Controls\Image
	 */
	public function addImage($name, $columnCallback)
	{
		$name = $this->translateByName($name);
		$control = new Controls\Image($name, $columnCallback);
		$control->setSorting();
		return $this->controls[$name] = $control;
	}

	/**
	 * akce pro radek
	 * @param string $name
	 * @return \EaredSeal\DataGrid\Controls\Action
	 */
	public function addAction($name)
	{
		$this['action']->setName($this->translateByName($name));
		$this->controls['action'] = $this['action'];
		return $this['action'];
	}

	/**
	 * Nastaveny form se pouzije pri zobrazeni dialogu na editaci zaznamu
	 * - ID controleru se musi shodovat s definici gridu, jinak nedojde k naplneni
	 * @param \Nette\Application\UI\Form $form
	 */
	public function addEditFormFactory(\Nette\Application\UI\Form $form)
	{

	}

	/**
	 * puzije se pri editaci formu
	 * @param type $name
	 * @param type $id
	 */
	public function addHiddenColumn($name, $id)
	{

	}

	/**
	 * sets of Columns
	 * @return array
	 */
	public function getControls()
	{
		return $this->controls;
	}

	private function translateByName($name)
	{
		if($name === '#')
		{
			return $name;
		}
		return $this->translator ? $this->translator->translate($name) : $name;
	}
}