<?php

namespace EaredSeal\DataGrid\Type;

use Nette\Application\UI,
	EaredSeal\DataGrid;

/**
 * jednoduchy vypis
 */
class Simple extends UI\Control
{
	/** @var data */
	private $file;
	/** @var \Iterator*/
	private $data;

	/** @var string */
	private $title;

	/** $var bool */
	private $withoutHeader;

	/** @var DataGrid\Container */
	private $container;

	private $sortableTarget;

	private $searchTarget;

	/** @var DataGrid\Sorting\Rules */
	protected $sortingRules;

	public function setPathFileTemplate($file)
	{
		$this->file = $file;
	}

	public function setData( $data)
	{
		$this->data = $data;
	}

	public function setTitle($text)
	{
		$this->title = $text;
	}

	public function setWithoutHeader()
	{
		$this->withoutHeader = TRUE;
	}

	public function setSortable($target)
	{
		$this->sortableTarget = $target;
	}

	/**
	 * @param string $link  napr. Homepage:edit!
	 */
	public function setLinkSearch($link, $controlPath, array $linkParam, $ajax = FALSE)
	{
		$this->searchTarget = (object) [
			"link" => $link,
			"param" => $linkParam,
			"controlPath" => $controlPath,
			"isAjax" => $ajax,
		];
	}

	public function setSortingRules(DataGrid\Sorting\Rules $rules = NULL)
	{
		$this->sortingRules = $rules;
	}

	/**
	 * Nastaveni radku
	 * @param Container $container
	 * @return void
	 */
	public function setCollum(DataGrid\Container $container)
	{
		$this->container = $container;
	}

	/**
	 * Renders template to output.
	 * @return void
	 */
	public function render()
	{
		$template = $this->getTemplate();
		$template->setFile($this->file ?: __DIR__ . "/Simple.latte");
		$template->data = $this->data;
		$template->container = $this->container;
		$template->title = $this->title;
		$template->withoutHeader = $this->withoutHeader;
		$template->rowName = substr(get_class($this->getParent()->getParent()), -6, 6);
		$template->sortableTarget = $this->sortableTarget;
		$template->searchTarget = $this->searchTarget;
		$template->sortingRules = $this->sortingRules;
		$template->render();
	}
}