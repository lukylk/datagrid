<?php

namespace EaredSeal\DataGrid\Sorting;

use Nette\Utils\Strings;
use EaredSeal\DataGrid\Controls\Link;

/**
 * @todo multi-order
 */
class Rules
{

	/** @var array */
	protected $items;

	/** @var string */
	protected $orderBy;

	/** @var bool */
	protected $order;

	/** @var array */
	protected $link;
	protected $controlPath;

	/** @var \Nette\Application\UI\Presenter */
	protected $presenter;

	public function __construct(\Nette\Application\UI\Presenter $presenter)
	{
		$this->presenter = $presenter;
	}

	/**
	 * @return \Nette\Application\UI\Presenter
	 */
	public function getPresenter()
	{
		return $this->presenter;
	}

	/**
	 * add rule to orderBy
	 * @param string $name
	 * @param string $orderBy
	 * @return self
	 *
	 * @todo add param \Closure $orderBy
	 */
	public function add($name, $orderBy)
	{
		$this->items[$this->createName($name)] = $orderBy;
		return $this;
	}

	/**
	 * @param string $name
	 * @param string $order
	 * @return \EaredSeal\DataGrid\Sorting\Rules
	 */
	public function setActualOrderBy(& $name, & $order, $controlPath)
	{
		$this->orderBy = $this->createName($name);
		$this->order = $order !== "desc" ? TRUE : FALSE;
		$this->controlPath = $controlPath;
		return $this;
	}

	/**
	 * @param string $name
	 * @param string $link  napr. Homepage:edit!
	 * @param string|\Closure $columnCallback
	 * @return \EaredSeal\DataGrid\Sorting\Rules
	 */
	public function setLink($link, $param = [])
	{
		$this->link = ["link" => $link, "param" => $param];
		return $this;
	}

	/**
	 * @return \EaredSeal\DataGrid\Controls\Link
	 */
	public function getLink($name)
	{
		$action = new Link("orderBy", function () use ($name) {
			return $this->getLinkParam($name) + (!empty($this->link["param"]) ? $this->link["param"] : []);
		});
		//$action->setPresenter($this->presenter);
		$action->setParent($this);
		$action->setLink($this->link["link"] ? : "this");

		return $action;
	}

	/**
	 * for generate link
	 * @param string $name
	 * @return array
	 */
	public function getLinkParam($name)
	{
		$key = $this->createName($name);
		if(isset($this->items[$key]))
		{
			return [
				$this->createParamName("orderby") => $key,
				$this->createParamName("order") => $this->orderBy === $key && $this->order ? "desc" : "asc",
			];
		}
		return [
			$this->createParamName("orderby") => NULL,
			$this->createParamName("order") => NULL,
		];
	}

	/**
	 * @param string $name
	 * @return bool
	 */
	public function hasOrderBy($name)
	{
		return $this->getLinkParam($name)[$this->createParamName("orderby")] !== NULL;
	}

	/**
	 * @param string $name
	 * @return string|NULL
	 */
	public function hasOrder($name)
	{
		if($this->orderBy === $this->createName($name))
		{
			return $this->getLinkParam($name)[$this->createParamName("order")] === "desc" ? "asc" : "desc";
		}
		return NULL;
	}

	/**
	 * rule for orderBy ORM
	 * @return array
	 */
	public function getRule()
	{
		if(isset($this->items[$this->orderBy]))
		{
			return [
				$this->items[$this->orderBy] => $this->order ? "ASC" : "DESC"
			];
		}
		return [];
	}

	private function createName($name)
	{
		return Strings::webalize($name) ? : "0";
	}

	private function createParamName($name)
	{
		return ($this->controlPath ? $this->controlPath . "-" : NULL) . $name;
	}

}