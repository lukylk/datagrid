<?php

namespace EaredSeal\DataGrid\Attributes;

/**
 * - orderBy for SortingRules
 * - search for LinkSearch
 */
trait Persistent
{

	/**
	 * @persistent
	 * @var string for SortingRules
	 */
	public $orderby;

	/**
	 * @persistent
	 * @var string for SortingRules
	 */
	public $order;

	/**
	 * @persistent
	 * @var string for LinkSearch
	 */
	 public $search;

	 protected function getLinkParam()
	 {
		 $path = $this->lookupPath();
		 return [
			$path . "-orderby" => $this->orderby,
			$path . "-order" => $this->order,
			$path . "-search" => $this->search,
		];
	 }

	 public function getSearchQuery()
	 {
		 return $this->search;
	 }
}