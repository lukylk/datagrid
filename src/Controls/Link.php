<?php

namespace EaredSeal\DataGrid\Controls;

use Nette;

/**
 * zobrazeni odkazu
 */
class Link extends Base
{
	/** @var string */
	private $link;
	/** @var array */
	private $linkParam;
	/** @var string */
	private $confirmText = NULL;
	/** @var string */
	private $dividerClass;
	/** @var array atributy doplnene k linku */
	private $attribute = [];
	/** @var Action */
	private $action;
	/** @var bool */
	private $isAjax;

	public function setParent($action)
	{
		$this->action = $action;
	}

	public function setLink($link)
	{
		$this->link = $link;
	}

	public function getUrl($row)
	{
		return $this->action->getPresenter()->link($this->link, (array) $this->getColumn($row) + (array) $this->linkParam);
	}

	public function setAjax()
	{
		$this->isAjax = TRUE;
	}

	public function isAjax()
	{
		return strpos($this->link, "!") !== FALSE || $this->isAjax;
	}

	public function setConfirm($text)
	{
		$this->confirmText = $text;
		return $this;
	}

	public function getConfirm()
	{
		return $this->confirmText;
	}

	public function setDividerClass($class = "divider")
	{
		$this->dividerClass = $class;
		return $this;
	}

	public function getDividerClass()
	{
		return $this->dividerClass;
	}

	/**
	 * ikona pred odkazem
	 * @param string $className
	 * @return self
	 */
	public function setIco($className)
	{
		return parent::setIco($className);
	}

	public function getAttributes()
	{
		return $this->attribute;
	}

	/**
	 * doplnenit k linku atribut
	 *
	 * @param string $attr
	 * @return \EaredSeal\DataGrid\Controls\Link
	 */
	public function addAttribute($attr)
	{
		$this->attribute[] = $attr;
		return $this;
	}

	/**
	 * nacist ajax do modal dialogu
	 * @return bool
	 */
	public function addModal()
	{
		$this->isAjax = TRUE;
		return $this->addAttribute('data-nette-modal="true"');
	}

	public function addLinkParam(array $param)
	{
		$this->linkParam = $param;
	}
}
