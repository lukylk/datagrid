<?php

namespace EaredSeal\DataGrid\Controls;

use Nette\Utils\Html;

/**
 * @author luky
 */
class Image extends Base
{

	/** @var array */
	protected $defaultAttributes = [
		"height" => "40px",
	];

	/**
	 * attributy pro \Nette\Utils\Html
	 * @param array $attributes
	 * @param bool $only
	 * @return \EaredSeal\DataGrid\Controls\Image
	 */
	public function setAttributes(array $attributes, $only = FALSE)
	{
		if($only)
		{
			$this->defaultAttributes = $attributes;
		}
		else
		{
			$this->defaultAttributes = array_merge($this->defaultAttributes, $attributes);
		}
		return $this;
	}

	public function getColumn($data)
	{
		$fileImage = parent::getColumn($data);
		if($fileImage)
		{
			return Html::el('img src="' . $fileImage . '"')->addAttributes($this->defaultAttributes);
		}

		return NULL;
	}

}