<?php

namespace EaredSeal\DataGrid\Controls;

use Nette\Utils\Html;

/**
 * @author luky
 */
class Text extends Base
{
	/** @var \Nette\Utils\Html */
	private $design;

	/**
	 * re-design value
	 * @param string $css
	 * @param string|\Nette\Utils\Html $element wrap element
	 */
	public function setDesign($css, $element = "span")
	{
		if($css === FALSE)
		{
			$this->design = NULL;
			return;
		}
		$this->design = $element instanceof Html ? $element : Html::el($element);
		$this->design->class = $css;
	}

	public function getColumn($data)
	{
		$column = parent::getColumn($data);

		if($this->design)
		{
			$this->design->setText($column);
			return $this->design;
		}
		return $column;
	}
}