<?php

namespace EaredSeal\DataGrid\Controls;

/**
 * @author luky
 */
class DateTime extends Base
{
	/** @var string */
	protected $format;

	/** @var string */
	protected $altText;

	public function setFormat($format, $altText = null)
	{
		$this->format = $format;
		$this->altText = $altText;
		return $this;
	}

	public function getColumn($data)
	{
		$dateTime = parent::getColumn($data);

		if($dateTime instanceof \DateTime)
		{
			return $dateTime->format($this->format);
		}

		return $this->altText;
	}
}
