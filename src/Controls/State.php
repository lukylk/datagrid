<?php

namespace EaredSeal\DataGrid\Controls;

use Nette\Utils\Html;

/**
 * @author luky
 */
class State extends Base
{
	public $string_on = "on";
	public $string_off = "off";

	private $rules = array();

	public function __construct($name, $columnCallback)
	{
		parent::__construct($name, $columnCallback);

		// default State
		$this->string_off = Html::el("span", ['class' => 'label label-danger'])->setText("deactive");
		$this->string_on = Html::el("span", ['class' => 'label label-success'])->setText("active");
	}

	public function getColumn($row)
	{
		$state = parent::getColumn($row);

		if(count($this->rules))
		{
			return $this->executeRules($row, $state);
		}
		elseif(is_string($this->column) && is_bool($state))
		{
			if($state === TRUE)
			{
				return $this->string_on;
			}
			else
			{
				return $this->string_off;
			}
		}
		else
		{
			return $state;
		}
	}

	public function getRules()
	{
		return $this->rules;
	}

	/**
	 * pravidla vyhodnoceni stavu
	 * - function($state, $row) {}
	 * @param \Closure $rule
	 * @return State
	 */
	public function setRules(\Closure $rule)
	{
		$this->rules[] = $rule;
		return $this;
	}

	/**
	 * aplikovani pravidel na data a ziskani vysledneho State
	 * @param object $row
	 * @param mix $state
	 * @return mix
	 */
	public function executeRules($row, $state)
	{
		foreach ($this->rules AS $rule)
		{
			$response = $rule($state, $row);
			if($response !== FALSE)
			{
				return $response;
			}
		}
		return $state;
	}
}