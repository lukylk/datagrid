<?php

namespace EaredSeal\DataGrid\Controls;

use Nette;

/**
 * @todo extends Nette\ComponentModel\Component implements IControl
 */
abstract class Base
{

	use Nette\SmartObject;

	/** @var string|\Closure */
	protected $column;

	/** @var string column name */
	private $name;

	/** @var string */
	private $width = NULL;

	/** @var string */
	private $icoClassName = NULL;

	/** @var bool */
	private $canBeSorted = TRUE;

	public function __construct($name, $columnCallback)
	{
		if(!is_string($columnCallback) && !$columnCallback instanceof \Closure)
		{
			throw new Nette\InvalidArgumentException("spatny datovy typ sloupce, je povoleny 'string' nebo 'Closure'");
		}
		$this->column = $columnCallback;
		$this->name = $name;
	}

	/**
	 * nazev sloupce
	 * @return string
	 */
	public function getName()
	{
		return $this->name;
	}

	/**
	 * vrati funkci pro ziskani hodnoty sloupce, napr. z SQL response
	 * @return mix
	 */
	public function getColumn($data)
	{
		if(is_string($this->column))
		{
			if(substr($this->column, -2, 2) == "()")
			{
				if(strpos($this->column, "->") !== FALSE)
				{
					$object = $data;
					foreach(explode("->", $this->column) as $item)
					{
						if(!$object)
						{
							return NULL;
						}
						$method = strpos($item, "()") !== FALSE ? substr($item, 0, -2) : $item;
						$object = $object->{$method}();
					}
					return $object;
				}
				$method = substr($this->column, 0, -2);
				return $data->{$method}();
			}
			else
			{
				return $data->{$this->column};
			}
		}
		else
		{
			// exec Closure
			$column = $this->column;
			return $column($data, $this);
		}
	}

	/**
	 *
	 * @return string|NULL
	 */
	public function getColumnForOrderBy()
	{
		if(is_string($this->column))
		{
			$items = explode(".", str_replace(["()", "->"], ["", "."], $this->column));
			foreach($items as & $item)
			{
				$item = substr($item, 0, 3) === "get" ? substr($item, 3) : $item;
				$item = substr($item, 0, 2) === "is" ? substr($item, 2) : $item;
				$item = substr($item, 0, 3) === "has" ? substr($item, 3) : $item;
				$item = Nette\Utils\Strings::firstLower($item);
			}
			return implode(".", $items);
		}
	}

	/**
	 * sirka sloupce
	 * @param string $size
	 * @return this
	 */
	public function setWidth($size)
	{
		$this->width = $size;
		return $this;
	}

	public function getWidth()
	{
		return $this->width;
	}

	/**
	 * ikona pred odkazem
	 * @param string $className
	 * @return Base
	 */
	public function setIco($className)
	{
		$this->icoClassName = $className;
		return $this;
	}

	public function getIco()
	{
		return $this->icoClassName;
	}

	/**
	 * ignorovani sloupce pro razeni
	 * @param bool $ignore
	 */
	public function setSorting($ignore = TRUE)
	{
		$this->canBeSorted = !$ignore;
		return $this;
	}

	/**
	 * @return bool
	 */
	public function canBeSorted()
	{
		return $this->canBeSorted;
	}

}