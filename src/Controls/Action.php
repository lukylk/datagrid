<?php

namespace EaredSeal\DataGrid\Controls;

use Nette,
	Nette\Utils,
	Kdyby\Translation\ITranslator;

/**
 * Nastaveni ovladacich akci
 */
class Action extends Nette\Application\UI\Control
{
	const TypeMenu_Dropdown = 0;
	const TypeMenu_ButtonGroup = 1;
	const TypeMenu_ButtonGroupOnlyIcon = 2;

	private $items = array();

	/** @var string Template Path */
	private $file;

	/** @var string css */
	private $buttonSize = "";

	/** @var string template name */
	private $typeMenu = "Dropdown";

	/** @var bool show text inside button */
	private $showName = TRUE;

	/** @var string Cell name */
	private $name;

	/** @var string Cell width */
	private $width;

	/** @var Nette\Application\IPresenter */
	private $presenter;

	/** @var \Kdyby\Translation\ITranslator */
    private $translator;

	public function setPresenter(Nette\Application\IPresenter $presenter)
	{
		$this->presenter = $presenter;
	}

	public function setTranslator(ITranslator $translator = NULL)
	{
		$this->translator = $translator;
	}

	/**
	 * Sets the path to the template file.
	 * @return self
	 */
	public function setFile($file)
	{
		$this->file = $file;
		return $this;
	}

	/**
	 * name buttonu
	 * @param string $text
	 * @return self
	 */
	public function setName($text)
	{
		$this->name = $text;
		return $this;
	}

	/**
	 * Add .btn-lg, .btn-sm, or .btn-xs for additional sizes
	 * @param string $css
	 * @return self
	 */
	public function setButtonSize($css)
	{
		$this->buttonSize = $css;
		return $this;
	}

	/**
	 * selecting a template
	 * @param int $typeMenu
	 *	0 - Dropdown (default)
	 *	1 - ButtonGroup
	 *	2 - ButtonGroup button without name
	 * @return self
	 */
	public function setTypeMenu($typeMenu)
	{
		switch($typeMenu)
		{
			case 1:
				$this->typeMenu = "ButtonGroup";
				break;
			case 2:
				$this->typeMenu = "ButtonGroup";
				$this->showName = FALSE;
				break;
			default:
				$this->typeMenu = "Dropdown";
				break;
		}
		return $this;
	}

	/**
	 *
	 * @param string $name
	 * @param string $link  napr. Homepage:edit!
	 * @param string|\Closure $columnCallback
	 * @return \EaredSeal\DataGrid\Controls\Link
	 */
	public function addLink($name, $link, $columnCallback = "id")
	{
		$name = $this->translator ? $this->translator->translate($name) : $name;

		$action = new Link($name, $columnCallback);
		$action->setParent($this);
		$action->setLink($link);
		$this->items[Utils\Strings::toAscii($name)] = $action;
		return $action;
	}

	/**
	 * Name linku
	 * @param string $name
	 * @return \EaredSeal\DataGrid\Controls\Action
	 */
	public function unSetLink($name)
	{
		unset($this->items[Utils\Strings::toAscii($name)]);
		return $this;
	}

	public function getName()
	{
		return $this->name;
	}

	/**
	 * sirka sloupce
	 * @param string $size
	 * @return Action
	 */
	public function setWidth($size)
	{
		$this->width = $size;
		return $this;
	}

	public function getWidth()
	{
		return $this->width;
	}

	public function getColumn($row)
	{
		$this->render($row);
	}

	public function render($row)
	{
		$template = $this->getTemplate();
		$template->setFile( $this->file ?: __DIR__ . DIRECTORY_SEPARATOR . "Action" . DIRECTORY_SEPARATOR . $this->typeMenu . ".latte");
		$template->items = $this->items;
		$template->row = $row;
		$template->buttonSize = $this->buttonSize;
		$template->showName = $this->showName;
		$template->render();
	}

	/**
	 * @return boolean
	 */
	public function canBeSorted()
	{
		return FALSE;
	}

	public function addLinkParam(array $param)
	{
		foreach($this->items as $item)
		{
			$item->addLinkParam($param);
		}
	}
}