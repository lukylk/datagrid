<?php

namespace EaredSeal\DataGrid;

interface IGrid
{
	/**
	 * @return Grid
	 */
	public function create();
}