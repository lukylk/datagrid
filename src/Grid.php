<?php

namespace EaredSeal\DataGrid;

use Nette,
	Kdyby\Translation\ITranslator;

/**
 * @author Lukas Kraus
 */
class Grid extends Nette\Application\UI\Control
{

	use Attributes\Persistent;

	/** @var callable[] */
	public $onAttached;

	/** @var callable[] */
	public $onRender;

	protected $data = array();

	/** @var string */
	protected $template;

	/** @var Container */
	protected $container;

	/** @var Type\Simple */
	protected $type;

	/** @var Sorting\Rules */
	protected $sortingRules;

	/** @var bool */
	protected $stopSorting;

	/** @var \Kdyby\Translation\ITranslator */
	protected $translator;

	public function createComponentType()
	{
		return new Type\Simple;
	}

	public function createComponentContainer()
	{
		return new Container;
	}

	/**
	 * template for DataGrid
	 * @param string $file
	 */
	public function setTemplate($file)
	{
		$this->template = $file;
		return $this;
	}

	/**
	 * ResultSet for DataGrid
	 * @param iterate $resultSet
	 * @param \EaredSeal\DataGrid\Sorting\Rules $rules
	 * @return self
	 */
	public function setData($resultSet, Sorting\Rules $rules = NULL)
	{
		$this->data = $resultSet;
		if(!empty($rules))
		{
			$this->setSortingRules($rules);
		}
		return $this;
	}

	/**
	 * Nadpis tabulky
	 * @param string $text
	 * @return self
	 */
	public function setTitle($text)
	{
		$this['type']->setTitle($text);
		return $this;
	}

	public function setWithoutHeader()
	{
		$this['type']->setWithoutHeader();
		return $this;
	}

	public function setSortable($target)
	{
		$this['type']->setSortable($target);
		return $this;
	}

	/**
	 *
	 * @param string $target napr. Homepage:list or this
	 * @param array $params
	 * @param bool $ajax
	 * @return \EaredSeal\DataGrid\Grid
	 */
	public function setLinkSearch($target = "this", $params = [], $ajax = FALSE)
	{
		$this->onRender[] = function () use ($target, $params, $ajax) {
			$link = $ajax ? $this->presenter->link($target, [$this->lookupPath()."-search" => NULL]) : $this->presenter->link($target);

			$this['type']->setLinkSearch($link, $this->lookupPath(), $this->getLinkParam() + $params, $ajax);
		};
		return $this;
	}

	/**
	 * @param \EaredSeal\DataGrid\Sorting\Rules $rules
	 * @return \EaredSeal\DataGrid\Grid
	 */
	public function setSortingRules(Sorting\Rules $rules)
	{
		$rules->setActualOrderBy($this->orderby, $this->order, $this->lookupPath());
		$this->sortingRules = $rules;
		return $this;
	}

	public function setStopSorting()
	{
		$this->stopSorting = TRUE;
	}

	public function setTranslator(ITranslator $translator)
	{
		$this->translator = $translator;
	}

	/**
	 * @return \EaredSeal\DataGrid\Sorting\Rules|NULL
	 */
	public function getSortingRules()
	{
		return $this->sortingRules;
	}

	/**
	 * Nastaveni radku
	 * @return \EaredSeal\DataGrid\Container
	 */
	public function getContainer()
	{
		$this['container']->setTranslator($this->translator);

		return $this['container'];
	}

	/**
	 * @param array $fields
	 * @param array $fieldMappings
	 * @return Container
	 */
	public function createContainer(array $fields, array $fieldMappings = [])
	{
		$container = $this->getContainer();
		foreach($fields as $item)
		{
			if(isset($fieldMappings[$item]) && $fieldMappings[$item] === 'datetime')
			{
				$container->addDateTime($item, $item)->setFormat("d.m.Y H:i:s");
			}
			elseif(isset($fieldMappings[$item]) && $fieldMappings[$item] === 'date')
			{
				$container->addDateTime($item, $item)->setFormat("d.m.Y");
			}
			else
			{
				$container->addText($item, $item);
			}
		}

		return $container;
	}

	public function render()
	{
		/* @var $type Type\Simple */
		$type = $this['type'];
		$type->setPathFileTemplate($this->template);
		$type->setData($this->data);
		$type->setCollum($this['container']);
		$type->setSortingRules($this->sortingRules);

		$this->onRender();
		$type->render();
	}

	public function renderTitle($text)
	{
		$type = $this['type'];
		$type->setTitle($this->translator->translate($text));
		$this->render();
	}

	private function createSortingRules()
	{
		$rules = new Sorting\Rules($this->presenter);
		foreach($this['container']->getControls() as $key => $control)
		{ /* @var $control Controls\Base */
			if(!$control->canBeSorted() || $this->stopSorting)
			{
				continue;
			}
			$rules->add($control->getName(), $control->getColumnForOrderBy());
		}
		$this->setSortingRules($rules);
	}

	/**
	 * This method will be called when the component (or component's parent)
	 * becomes attached to a monitored object. Do not call this method yourself.
	 * @param  Nette\ComponentModel\IComponent
	 * @return void
	 */
	protected function attached($presenter)
	{
		parent::attached($presenter);
		$this->translator = $this->translator ?: $presenter->translator;
		// nastavy pravidla pro razeni
		$this->createSortingRules();
		// doplneni persistent parametru pro odkazy na [Presenter:]action
		$this['container-action']->addLinkParam($this->getLinkParam());

		$this->onAttached();
	}

}